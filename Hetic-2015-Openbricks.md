# Hetic 2015 / Openbricks.io

Dans le but de s'intégrer dans les développements existants de [www.openbricks.io](www.openbricks.io), tout en gardant une autonomie de développement à l'équipe d'étudiants de Hetic, nous avons choisis de travailler sur les points suivants.

***

## Design UI + UX

### Objectifs
* Définir un UI Kit global des éléments d'interface du site

### Elements de départs
* Site existant en ligne http://www.openbricks.io/
* Wireframes : https://moqups.com/lucas.sebastien@gmail.com/6antp4cF

### Points à traiter

* Analyse de la charte graphique existante et suggestion
* Découpage de l'application en élements d'interface
* Analyse des intéractions souhaitées et proposition d'interfaces
* Design des élements
* Export du UI KIT

***

## Développement

**Méthode** :

* Partir du starter https://github.com/linnovate/mean (node.js, angular.js, mongodb...)
* Coté front configurer l'api pour dialoguer avec l'api existante sur un domaine que nous préciserons (à l'exception des nouvelles routes d'api qui pointerons vers les nouvelles routes d'api développé par le backend)
* Reproduire rapidement la page de liste de briques existantes sans rentrer dans le détail
* Certaines parties de l'existant seront rendus disponibles et chargés comme des librairies grace à bower ou git submodule (par exemple la librairies de mixins jade...). On determinera ce qui est nécessaire au fur et à mesure.
* Développement des éléments demandés dans une logique de librairie, c'est à dire pouvant être inséré facilement dans le site actuel comme un module (utilisation de directive angular, espace de nom css...)
* On pourra également utiliser le système de module de mean.io : la fenêtre modale et le panneau de gauche sont des modules qui viennent se rajouter à l'application de base en lui apportant de nouvelles fonctionnalités

***

## Développement backend

### Objectif

* Développement de l'API existante
* Scrapping et lecture de flux RSS

### Elements de départs

* L'api existante

### Point travaillés

* Ajout de routes d'api et structure de donnée mongodb pour les points ajoutés par l'équipe front
  * gestion des commentaires
  * gestion du réordonnement elements
  * gestion des mises en favoris
* Mise en place d'un moteur de scrapping et de lecture de  flux RSS pour venir proposer des flux externes
  * Synchronisation dans le sens Pinterest vers Openbricks > sauvegarde des pins comme bricks
  * Scrapping de plateformes d'architecture > affichage d'images d'architecture dans un mini moteur de recherche intégré dans le volet de gauche

***

## Développement frontend

### Objectif

* Développement de la modal d'une brick (inspiré de Trello)
* Développement du panneau de gauuche (inspiré de Canva)

### Elements de départs

* La base de ces élements déjà développés

### Point travaillés

**Modal et full screen : (dev front 1)** :

* Le navigateur de brick (Brick parent, enfant, en relation...)
* La gestion des utilisateurs
* Les commentaires
* Les fichiers attachés
* ...
* En fonction du temps, on pourra travailler sur l'interface "fullscreen"...

**Side panel et vue liste :**

* Le panneau de gauche
* Le navigateur de calque
* La recherche de brick interne + externe (lecteur de flux)
* Le panneau de séléction
* ....
* En fonction du temps, on pourra travailler sur la vue liste elle-même, le widget de copie d'une brique....

***

## Marketing

### Objectifs
* Elaboration d'une stratégie marketing globale

### Points
* **Analyse de la cible** : les architectes et créatifs visuels (designer, web designer, photographes...)
* **Analyse de la concurrence** : en particulier dradgis ou autres applications de collectes et d'organisation d'images / Analyser plus particulièrement :
  * les fonctionnalités de l'application
  * Comment elle se fait connaître auprès de sa cible ?
  * Ses principales métriques
  * Quelles sont les fonctionnalités clés mis en avant ?
  * Quel est le business model ?
  * ...
* Quelle est la différentiation de Openbricks ? Ses fonctionnalités clés ?
* Les différents canaux pour se faire connaître
* Mise en place d'une stratégie de communication
  * Stratégie virale : invitation, parainage, partages réseaux sociaux
  * Stratégie SEO incluant le contenu utilisateur
  * Stratégie relation presse (trés important pour les apps)

